"""
Controller file that communicates with the model and view files
"""

__author__ = "Samer El Khoury"

from view_obj import View
from model import Ensemble
from model import Person

class Controller():
    """
    Creates a class controller with 2 attributes view and model
    """
    def __init__(self):
        self.view = View(self)
        self.model = Ensemble()

    def start_view(self):
        """
        creates tkinter widgets
        """
        self.view.create_fields()
        self.view.main()

    def search(self):
        """
        searches for a person using his name
        """
        searched_name, searched_list = self.model.search_person(self.view.get_value("Nom"))
        self.view.search_view(searched_name, searched_list)

    def delete(self):
        """
        deletes the person using his name
        """
        del_name = self.view.get_value("Nom")
        self.model.delete_person(del_name)

    def insert(self):
        """
        inserts the person's infos
        """
        person = Person(self.view.get_value("Nom"), self.view.get_value("Prenom"),
                        self.view.get_value("Telephone"), self.view.get_value("Adresse"),
                        self.view.get_value("Ville"))
        print(person)

        self.model.insert_person(person)

    def button_press_handle(self, button_id):
        """
        launches a function depending on which button is pressed
        """
        print("[Controller][button_press_handle] "+ button_id)
        if button_id == "Chercher":
            self.search()
        elif button_id == "Effacer":
            self.delete()
        elif button_id == "Inserer":
            self.insert()
        else:
            pass

if __name__ == "__main__":
    CONTROLLER = Controller()
    CONTROLLER.model.load_file()
    CONTROLLER.start_view()
    CONTROLLER.model.save_file()

"""
Class View of the project

@author : Olivier CHABROL
"""

from tkinter import Button
from tkinter import Entry
from tkinter import Label
from tkinter import StringVar
from tkinter import Tk
from tkinter import messagebox


class View(Tk):
    """
    tkinter interface
    """
    def __init__(self, controller):
        super().__init__()
        self.controller = controller
        self.widgets_labs = {}
        self.widgets_entry = {}
        self.widgets_button = {}
        self.entries = ["Nom", "Prenom", "Telephone", "Adresse", "Ville"]
        self.buttons = ["Chercher", "Inserer", "Effacer"]

    @staticmethod
    def search_view(searched_name, searched_list):
        """
        shows warnings and results
        """
        if not searched_list:
            messagebox.showwarning(title="WARNING", message=searched_name)
        else:
            names = "\n - ".join(searched_list)
            messagebox.showinfo(title="Result", message=searched_name + "\n - "+ names)

    def get_value(self, key):
        """
        gets the entry
        """
        return self.widgets_entry[key].get()

    def create_fields(self):
        """
        creates buttons and labels
        """
        i, j = 0, 0

        for idi in self.entries:
            lab = Label(self, text=idi.capitalize())
            self.widgets_labs[idi] = lab
            lab.grid(row=i, column=0)

            var = StringVar()
            entry = Entry(self, text=var)
            self.widgets_entry[idi] = entry
            entry.grid(row=i, column=1)

            i += 1

        for idi in self.buttons:
            button_w = Button(self, text=idi, command=
                              (lambda button=idi: self.controller.button_press_handle(button)))
            self.widgets_button[idi] = button_w
            button_w.grid(row=i+1, column=j)

            j += 1

    def main(self):
        """
        window title
        """
        print("[View] main")
        self.title("Annuaire")
        self.mainloop()

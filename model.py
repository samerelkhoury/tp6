"""
model file where whe have all the information and will execute operations
"""

__author__ = "Samer El Khoury"
import pickle


class Person:
    """
    creates a person
    """
    def __init__(self, nom, prenom, telephone='', adresse='', ville=''):
        self.nom = nom
        self.prenom = prenom
        self.telephone = telephone
        self.adresse = adresse
        self.ville = ville

    def get_nom(self):
        """
        gets family name
        """
        return self.nom

    def get_prenom(self):
        """
        gets name
        """
        return self.prenom

    def get_telephone(self):
        """
        gets phone number
        """
        return self.telephone

    def get_adresse(self):
        """
        gets adress
        """
        return self.adresse

    def get_ville(self):
        """
        gets village
        """
        return self.ville

    def __str__(self):
        """
        return in string
        """
        return self.get_nom()+'\n'+self.get_prenom()+'\n'+self.get_telephone()+'\n'+self.get_adresse()+'\n'+self.get_ville()


class Ensemble:
    """
    creates a class for all persons
    """
    def __init__(self):
        self.list_person = {}

    def insert_person(self, person):
        """
        inserts person
        """
        if isinstance(person, Person):
            prenom = person.get_prenom()
            nom = person.get_nom()
            self.list_person[f"{prenom} {nom}"] = person

    def delete_person(self, nom):
        """
        deletes a person
        """
        values_to_del = []
        for element in self.list_person.keys():
            if nom in element:
                values_to_del.append(element)
        for to_del in values_to_del:
            del self.list_person[to_del]
            print('Deleted: '+to_del)

    def search_person(self, name):
        """
        searches for a person by his name
        """
        names_fetched = []
        if not name:
            return "Please Enter A Name", names_fetched

        for element in self.list_person.keys():
            if name in element:
                names_fetched.append(element)
        if not names_fetched:
            return "The Name Doesn't Exist !", names_fetched
        return f"The searched Name is : {name} ", names_fetched

    def save_file(self):
        """
        saves the file
        """
        with open("Saved List.pickle", "wb") as output_file:
            pickle.dump(self.list_person, output_file)

    def load_file(self):
        """
        loads the file
        """
        with open("Saved List.pickle", "rb") as input_file:
            self.list_person = pickle.load(input_file)

    def __str__(self):
        test = ''
        for element in self.list_person:
            test.join(element)
        return test
